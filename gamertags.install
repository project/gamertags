<?php
/**
 * @file
 * Installation file for the Gamertags project.
 */

/**
 * Implements hook_uninstall().
 */
function gamertags_uninstall() {
  $vars = array(
    // Module variables.
    'gamertags_on_registration',
    'gamertags_validate_tags',
    // Microsoft Xbox variables.
    'gamertags_xbox_collect',
    'gamertags_xbox_weight',
    'gamertags_xbox_url',
    // Nintendo 3DS variables.
    'gamertags_3ds_collect',
    'gamertags_3ds_weight',
    'gamertags_3ds_url',
    // Nintendo Mario Kart variables.
    'gamertags_mariokart_collect',
    'gamertags_mariokart_weight',
    'gamertags_mariokart_url',
    // Nintendo Wii variables.
    'gamertags_wii_collect',
    'gamertags_wii_weight',
    'gamertags_wii_url',
    'gamertags_use_mapwii',
    // Steam variables.
    'gamertags_steam_collect',
    'gamertags_steam_weight',
    'gamertags_steam_url',
    // Xfire variables.
    'gamertags_xfire_collect',
    'gamertags_xfire_weight',
    'gamertags_xfire_url',
    'gamertags_xfire_url_profiles',
    'gamertags_xfire_ask_skin',
    'gamertags_xfire_default_skin',
    'gamertags_xfire_ask_size',
    'gamertags_xfire_default_size',
    // Sony Playstation variables.
    'gamertags_psn_collect',
    'gamertags_psn_weight',
    'gamertags_psn_url',
    'gamertags_psn_url_us',
  );
  foreach ($vars as $var) {
    variable_del($var);
  }
}

/**
 * Removes now defunct MyGamerCard option.
 * Changes psn_eu_url variable to psn_url for the sake of SimpleTesting.
 * Following better naming convention for PSN US URL collection.
 */
function gamertags_update_7200() {
  variable_del('gamertags_mygamercard_url');
  variable_del('gamertags_debunk_ms');
  variable_set('gamertags_psn_url', variable_get('gamertags_psn_eu_url'));
  variable_del('gamertags_psn_eu_url');
  variable_set('gamertags_psn_url_us', variable_get('gamertags_psn_us_url'));
  variable_del('gamertags_psn_us_url');
}

/**
 * Update user entries for 7.1.x => 7.2.x upgrade.
 */
function gamertags_update_7201(&$sandbox) {
  // Set up for a multi-part update.
  if (!isset($sandbox['progress'])) {
    // Count users for processing (less one for the anonymous user).
    $sandbox['max'] = db_query('SELECT COUNT(uid) FROM {users}')->fetchField() - 1;
    $sandbox['progress'] = $sandbox['current_user'] = 0;
  }

  // Only process x$limitx accounts at a time.
  $limit = 50;
  $accounts = db_select('users', 'u')
    ->fields('u', array('uid', 'data'))
    ->condition('uid', $sandbox['current_user'], '>')
    ->range(0, $limit)
    ->orderBy('uid', 'ASC')
    ->execute();
  foreach ($accounts as $account) {
    $sandbox['current_user'] = $account->uid;
    $sandbox['progress']++;
    // Only act if there is serialized data.
    if ($data = drupal_unpack($account)) {
      // We don't want the uid in the data array.
      unset($data->uid);
      // Make this flag based, so unaltered accounts aren't updated.
      $altered = FALSE;
      // Process any Gamertags present.
      // Xbox...
      if (isset($data->xbox_gamertag)) {
        $altered = TRUE;
        $data->gamertags_xbox = $data->xbox_gamertag;
        unset($data->xbox_gamertag);
      }
      // Playstation Network...
      if (isset($data->psn_gamertag)) {
        $altered = TRUE;
        $data->gamertags_psn = $data->psn_gamertag;
        $data->gamertags_psn_region = $data->psn_region;
        unset($data->psn_gamertag);
        unset($data->psn_region);
      }
      // Wii...
      if (isset($data->wii_gamertag)) {
        $altered = TRUE;
        $data->gamertags_wii = $data->wii_gamertag;
        unset($data->wii_gamertag);
      }
      // Steam...
      if (isset($data->steam_gamertag)) {
        $altered = TRUE;
        $data->gamertags_steam = $data->steam_gamertag;
        unset($data->steam_gamertag);
      }

      // Update the user account if modified.
      if ($altered) {
        db_update('users')
          ->fields(array('data' => serialize($data)))
          ->condition('uid', $account->uid)
          ->execute();
      }
    }
  }

  // Inform the batch update engine that we are not finished.
  if ($sandbox['progress'] != $sandbox['max']) {
    $sandbox['#finished'] = $sandbox['progress'] / $sandbox['max'];
  }
  else {
    return t('Updated all user Gamertags.');
  }
}
