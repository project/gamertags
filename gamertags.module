<?php
/**
 * @file
 * Main module file for the Gamertags project.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function gamertags_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && !empty($plugin)) {
    return "plugins/$plugin";
  }
}

/**
 * Implements hook_filter_info().
 */
function gamertags_filter_info() {
  $platforms = array();
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (variable_get("{$class}_collect", TRUE) && method_exists($class, 'filterInfo')) {
      $platform = call_user_func(array($class, 'filterInfo'));
      $platforms[] = $platform['short'];
    }
  }
  return array(
    'filter_gamertags' => array(
      'title' => t('Gamertags filter'),
      'description' => t('Substitutes "@platforms" with the corresponding Gamertag.', array('@platforms' => implode(', ', $platforms))),
      'process callback' => 'gamertags_filter_gamertags_process',
      'tips callback' => 'gamertags_filter_gamertags_tips',
    ),
  );
}

/**
 * Implements hook_filter_FILTER_process().
 */
function gamertags_filter_gamertags_process($text, $filter, $format) {
  // Gather all the platform machine-names for the regexp.
  $platforms = array();
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (variable_get("{$class}_collect", TRUE)) {
      $platforms[] = $platform_info['name'];
    }
  }
  return preg_replace_callback("/\[\s*(" . implode('|', $platforms) . ")\s*:\s*(.*?)\s*\]/i", "_gamertags_process_filter", $text);
}

/**
 * Implements hook_filter_FILTER_tips().
 */
function gamertags_filter_gamertags_tips($filter, $format, $long = FALSE) {
  $platforms = array();
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (variable_get("{$class}_collect", TRUE) && method_exists($class, 'filterInfo')) {
      $platform = call_user_func(array($class, 'filterInfo'));
      $platforms[] = $long ? '<dd>' . check_plain($platform['long']) . '</dd>' : check_plain($platform['short']);
    }
  }
  if ($long) {
    return t('<dl><dt>You can display your gamertags using the following formats:</dt>!dd</dl>', array('!dd' => implode('', $platforms)));
  }
  return t('You can display gamertags using the following formats: "@platforms".', array('@platforms' => implode(', ', $platforms)));
}

/**
 * Implements hook_menu().
 */
function gamertags_menu() {
  $items['admin/config/people/gamertags'] = array(
    'title' => 'Gamertag Settings',
    'description' => 'Settings for the Gamertags module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gamertags_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'includes/gamertags.settings.inc',
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function gamertags_theme() {
  // Collect the theme template/ preprocess information for all platforms.
  $platforms = array();
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (method_exists($class, 'theme')) {
      $platforms[$class] = call_user_func(array($class, 'theme'));
    }
  }
  return array(
    'gamertags_settings_collect' => array(
      'render element' => 'form',
      'file' => 'includes/gamertags.settings.inc',
    ),
  ) + $platforms;
}

/**
 * Implements hook_token_info().
 */
function gamertags_token_info() {
  $tokens = array();
  if (_gamertags_test_any()) {
    $tokens['user-gamertags'] = array(
      'name' => t('Gamertags: All'),
      'description' => t('All Gamertags for the current user.'),
    );
    $tokens['user-gamertags-text'] = array(
      'name' => t('Gamertags: All (text)'),
      'description' => t('All Gamertags for the current user (as plain text).'),
    );
  }
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (variable_get("{$class}_collect", TRUE) && method_exists($class, 'tokenList')) {
      $tokens += call_user_func(array($class, 'tokenList'));
    }
  }
  return array(
    'tokens' => array('user' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */
function gamertags_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  if ($type == 'user' && !empty($data['user'])) {
    $sanitize = !empty($options['sanitize']);
    if (_gamertags_test_any()) {
      $replacements['[user:user-gamertags]'] = theme('gamertags_all', array('account' => $data['user'], 'as_text' => FALSE, 'sanitize' => $sanitize));
      $replacements['[user:user-gamertags-text]'] = theme('gamertags_all', array('account' => $data['user'], 'as_text' => TRUE, 'sanitize' => $sanitize));
    }
    foreach (gamertags_return_all() as $class => $platform_info) {
      if (variable_get("{$class}_collect", TRUE) && method_exists($class, 'tokenValues')) {
        // Only process the tokens if the user actually has the Gamertag.
        if (!empty($data['user']->data[$class])) {
          $replacements += call_user_func(array($class, 'tokenValues'), $data['user'], $sanitize);
        }
      }
    }
  }
  return $replacements;
}

/**
 * Implements hook_user_view().
 */
function gamertags_user_view($account, $view_mode, $langcode) {
  // Displays the Gamertag for each enabled platform.
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (variable_get("{$class}_collect", TRUE) && !empty($account->data[$class]) && method_exists($class, 'userView')) {
      call_user_func_array(array($class, 'userView'), array(&$account, $view_mode, $langcode));
    }
  }
}

/**
 * Implements hook_user_presave().
 */
function gamertags_user_presave(&$edit, $account, $category) {
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (variable_get("{$class}_collect", TRUE) && method_exists($class, 'userSave')) {
      call_user_func_array(array($class, 'userSave'), array(&$edit, $account, $category));
    }
  }
}

/**
 * Implements hook_views_api().
 */
function gamertags_views_api() {
  return array(
    'api' => 2.0,
    'path' => drupal_get_path('module', 'gamertags') . '/plugins',
  );
}

/**
 * Implement hook_form_alter().
 */
function gamertags_form_alter(&$form, &$form_state, $form_id) {
  if (($form_id == 'user_register_form' && variable_get('gamertags_on_registration', TRUE) || $form_id == 'user_profile_form') && in_array($form['#user_category'], array('account', 'register'))) {
    $account = $form['#user'];
    // Bail out here if none of the Gamertag information is set to be collectible.
    if (!_gamertags_test_any() || empty($account)) {
      return;
    }
    // Displays enabled platforms Gamertag entry form.
    $gamertags = array();
    foreach (gamertags_return_all() as $class => $platform_info) {
      if (variable_get("{$class}_collect", TRUE) && method_exists($class, 'userForm')) {
        $gamertags += call_user_func_array(array($class, 'userForm'), array($account));
      }
    }
    // Only include the fieldset and validation if there are Gamertags to display.
    if (!empty($gamertags)) {
      $form['gamertags'] = array(
        '#type' => 'fieldset',
        '#title' => t('Gamertags'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        '#weight' => 1,
      ) + $gamertags;
      $form['#validate'][] = 'gamertags_user_form_validate';
    }
  }
}

/**
 * Validation callback for the Gamertags user account form.
 */
function gamertags_user_form_validate(&$form, &$form_state) {
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (variable_get("{$class}_collect", TRUE) && !empty($form_state['values'][$class]) && method_exists($class, 'userFormValidate')) {
      call_user_func_array(array($class, 'userFormValidate'), array(&$form_state['values']));
    }
  }
}

/**
 * Helper function to return TRUE if any gamertags are set to be collectable/ displayable.
 */
function gamertags_return_all($reset = FALSE) {
  $gamertags = &drupal_static(__FUNCTION__, array());
  // If the list isn't already statically cached or we've been sent here to rebuild the list.
  if (empty($gamertags) || $reset) {
    // If this isn't a 'reset' then attempt to retrieve from the Drupal cache.
    if (!$reset && ($cache = cache_get('gamertags')) && !empty($cache->data)) {
      $gamertags = $cache->data;
    }
    // Else, rebuild...
    else {
      $gamertags = array();
      foreach (file_scan_directory(drupal_get_path('module', 'gamertags') . '/platforms', '/\.inc$/i') as $file) {
        // Class names should follow the gamertags_{platform_machine_name} convention.
        $class = str_replace('.', '_', $file->name);
        // Check the class exists and that it has a callable method of 'getInfo'.
        if (class_exists($class) && method_exists($class, 'getInfo')) {
          $info = call_user_func(array($class, 'getInfo'));
          $gamertags[$class] = $info;
        }
      }
      uksort($gamertags, 'strnatcasecmp');
      cache_set('gamertags', $gamertags);
    }
  }
  return $gamertags;
}

/**
 * Helper function to return TRUE if any gamertags are set to be collectable/ displayable.
 */
function _gamertags_test_any() {
  foreach (gamertags_return_all() as $class => $platform_info) {
    if (variable_get("{$class}_collect", TRUE)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Helper function which processes token matching and returns the appropriate Gamertag.
 */
function _gamertags_process_filter($match) {
  // $match[0] contains the unprocessed filter.
  // $match[1] contains the platform name.
  // $match[2] contains the arguments.
  $platform = $match[1];
  $class = "gamertags_{$platform}";
  $args = preg_split('/\s*:\s*/', $match[2], NULL, PREG_SPLIT_NO_EMPTY);
  // Error checking...
  if (count($args) == 0) {
    return t('[<span class="error">Empty gamertag parameter</span>]');
  }
  // If there is one argument (the gamertag) then output using a straight theme function.
  elseif (count($args) == 1) {
    return theme($class, array('gamertag' => $args[0]));
  }
  // Else, we have to parse the arguments as they differ between platforms.
  else {
    $theme_info = call_user_func(array($class, 'theme'));
    // Test if too many arguments have been provided.
    if (count($args) > count($theme_info['variables'])) {
      return t('[<span class="error">Too many arguments entered for this platform</span>]');
    }
    elseif (count($args) < count($theme_info['variables'])) {
      // Ensure the array_combine will be successful.
      $args = array_pad($args, count($theme_info['variables']), NULL);
    }
    $theme_args = array_combine(array_keys($theme_info['variables']), $args);
    return call_user_func_array('theme', array($class, $theme_args));
  }
}
